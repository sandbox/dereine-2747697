<?php

namespace Drupal\jump_menu\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a common implementation of a jump menu for other usage.
 */
class JumpMenuForm extends FormBase {

  use JumpMenuFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jump_menu';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $args = $form_state->getBuildInfo()['args'];
    $args += [1 => []];
    $form = $this->jumpMenuForm($form, $form_state, $args[0], $args[1]);
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
