<?php

namespace Drupal\jump_menu\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Generate a jump menu form.
 *
 * This can either be used with \Drupal::formBuilder()->getForm() or directly
 * added to a form. The button provides its own submit handler so by default,
 * other submit handlers will not be called.
 *
 * One note: Do not use #tree = TRUE or it will be unable to find the
 * proper value.
 *
 * @code
 * $form = \Drupal::formBuilder()->getForm(JumpMenuForm::class, $targets, $options);
 * @endcode
 *
 * To integrate it with your own custom form, look into what
 * \Drupal\jump_menu\Form\JumpMenuForm is doing.
 *
 * @param \Drupal\jump_menu\Form\JumpMenuOptions $select
 *   An array suitable for use as the #options. The keys will be the direct
 *   URLs that will be jumped to, so you absolutely must encode these using
 *   url() in order for them to work reliably.
 *
 * @param array $options
 *   $options may be an array with the following options:
 *   - 'title': The text to display for the #title attribute.
 *   - 'description': The text to display for the #description attribute.
 *   - 'default_value': The text to display for the #default_value attribute.
 *   - 'hide': If TRUE the go button will be set to hide via javascript and
 *     will submit on change.
 *   - 'button': The text to display on the button.
 *   - 'image': If set, an image button will be used instead, and the image
 *     set to this.
 *   - 'inline': If set to TRUE (default) the display will be forced inline.
 */
trait JumpMenuFormTrait {

  protected function jumpMenuForm($form, FormStateInterface $form_state, JumpMenuOptions $select, $options = []) {
    $options += [
      'button' => $this->t('Go'),
      'choose' => $this->t('- Choose -'),
      'inline' => TRUE,
      'hide' => TRUE,
    ];

    $form['#attributes']['data-jump-menu'] = 1;

    $form['#attached']['library'][] = 'jump_menu/jump-menu';

    if (!empty($options['choose'])) {
      $select->prependOption($options['choose'], Url::fromRoute('<none>'));
    }

    $form['jump'] = [
      '#type' => 'select',
      '#options' => $select->toSelectOptions(),
      '#attributes' => [
        'data-jump-menu-select' => 1,
      ],
    ];

    if (!empty($options['title'])) {
      $form['jump']['#title'] = $options['title'];
    }

    if (!empty($options['description'])) {
      $form['jump']['#description'] = $options['description'];
    }

    if (!empty($options['default_value'])) {
      $form['jump']['#default_value'] = $options['default_value'];
    }

    if (isset($options['image'])) {
      $form['go'] = [
        '#type' => 'image_button',
        '#src' => $options['image'],
        '#submit' => [[$this, 'jumpMenuSubmit']],
        '#attributes' => [
          'data-jump-menu-button' => 1,
        ],
      ];
    }
    else {
      $form['go'] = [
        '#type' => 'submit',
        '#value' => $options['button'],
        '#submit' => [[$this, 'jumpMenuSubmit']],
        '#attributes' => [
          'data-jump-menu-button' => 1,
        ],
      ];
    }

    if ($options['inline']) {
      $form['jump']['#prefix'] = '<div class="container-inline">';
      $form['go']['#suffix'] = '</div>';
    }

    if ($options['hide']) {
      $form['jump']['#attributes']['data-jump-menu-change'] = 1;
      $form['go']['#attributes']['class'][] = 'js-hide';
    }

    return $form;
  }

  /**
   * Submit handler for the jump menu.
   *
   * This is normally only invoked upon submit without javascript enabled.
   */
  public function jumpMenuSubmit($form, FormStateInterface $form_state) {
    if ($form_state->getValue('jump') === '') {
      // We have nothing to do when the user has not selected any value.
      return;
    }

    $redirect = $form_state->getValue('jump');

    // If the path we are redirecting to starts with the base path (for example,
    // "/somepath/node/1"), we need to strip the base path off before passing it
    // to $form_state['redirect'].

    // Allow more than one element pointing to the same location.
    if (strpos($redirect, '##')) {
      list($redirect, ) = explode('##', $redirect, 2);
    }

    // Parse the URL so that query strings and fragments are preserved in the
    // redirect.
    $form_state->setRedirectUrl(Url::fromUri($redirect));
  }

}
