<?php

namespace Drupal\jump_menu\Form;

use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;

class JumpMenuOptions {

  /**
   * Stores the available options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Creates a new JumpMenuOptions instance.
   *
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator.
   */
  public function __construct(UrlGeneratorInterface $url_generator) {
    $this->urlGenerator = $url_generator;
  }

  public static function create() {
    return new static(\Drupal::urlGenerator());
  }

  public function addOption($label, Url $url) {
    $this->options[] = [$label, $url];
    return $this;
  }

  public function prependOption($label, Url $url) {
    array_unshift($this->options, [$label, $url]);
    return $this;
  }

  public function toSelectOptions() {
    $options = [];
    array_walk($this->options, function ($option) use (&$options) {
      /** @var \Drupal\Core\Url $url */
      list($label, $url) = $option;
      $url_string = $url->setAbsolute(TRUE)->toString(TRUE)->getGeneratedUrl();

      // Allow to have more than one URL pointing to the same location.
      if (isset($options[$url_string])) {
        // Generate a deterministic but sort of random value.
        $url_string = $url_string . '##' . md5(serialize($options));
      }
      $options[$url_string] = $label;
    });
    return $options;
  }

}
