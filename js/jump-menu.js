(function($, window) {

  'use strict';

  Drupal.behaviors.JumpMenu = {
    attach: function(context) {
      $('[data-jump-menu]', context).each(function(elem) {
        var $select = $('[data-jump-menu-select]', elem);
        var $button = $('[data-jump-menu-button]', elem);
        var jumpMenu = new Drupal.jumpMenu($select, $button);
        jumpMenu.attach();
      });
    }
  };

  Drupal.jumpMenu = function ($jumpSelect, $jumpButton) {
    this.$jumpSelect = $jumpSelect;
    this.$jumpButton = $jumpButton;
  };

  Drupal.jumpMenu.prototype.attach = function () {
    if (this.$jumpSelect.attr('data-jump-menu-change')) {
      this.$jumpSelect.once().change(Drupal.jumpMenu.prototype.click.bind(this));
    }
    this.$jumpButton.once().click(Drupal.jumpMenu.prototype.click.bind(this));
  };

  /**
   * Instead of submitting the form, just perform a redirect.
   *
   * @returns {boolean}
   *   This function never returns, as the redirect is executed before.
   */
  Drupal.jumpMenu.prototype.click = function () {
    var loc = this.$jumpSelect.val();
    var urlArray = loc.split('##');
    if (urlArray[0]) {
      window.location.href = urlArray[0];
    }
    else {
      window.location.href = loc;
    }
    return false;
  };

})(jQuery, window);
