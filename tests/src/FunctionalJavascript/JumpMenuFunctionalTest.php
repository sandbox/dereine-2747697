<?php

namespace Drupal\Tests\jump_menu\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\JavascriptTestBase;

/**
 * Tests the jump menu.
 *
 * @see \Drupal\jump_menu\Form\JumpMenuFormTrait
 * @see \Drupal\jump_menu\Form\JumpMenuForm
 * @see \Drupal\jump_menu\Form\JumpMenuOptions
 *
 * @group jump_menu
 */
class JumpMenuFunctionalJavascriptTest extends JavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['jump_menu', 'test_page_test', 'jumpmenu_test'];

  public function testJumpMenu() {
    $this->markTestSkipped('This test does not work yet');
    $url_string = Url::fromRoute('test_page_test.test_page')
      ->setAbsolute()
      ->toString(TRUE)
      ->getGeneratedUrl();
    $this->drupalGet('/jump-menu-test');

    $js = <<<JS
  jQuery('#edit-jump').val($url_string);
  jQuery('#edit-jump').change();
JS;
    $this->getSession()->executeScript($js);

    $this->assertSession()->addressEquals($url_string);
  }

}
