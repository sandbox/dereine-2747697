<?php

namespace Drupal\Tests\jump_menu\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the jump menu.
 *
 * @see \Drupal\jump_menu\Form\JumpMenuFormTrait
 * @see \Drupal\jump_menu\Form\JumpMenuForm
 * @see \Drupal\jump_menu\Form\JumpMenuOptions
 *
 * @group jump_menu
 */
class JumpMenuFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['jump_menu', 'test_page_test', 'jumpmenu_test'];

  public function testJumpMenu() {
    $url_string = Url::fromRoute('test_page_test.test_page')
      ->setAbsolute()
      ->toString(TRUE)
      ->getGeneratedUrl();
    $this->drupalPostForm('/jump-menu-test', [
      'jump' => $url_string,
    ], t('Go'));

    $this->assertSession()->addressEquals($url_string);
  }

}
