<?php

namespace Drupal\jumpmenu_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Url;
use Drupal\jump_menu\Form\JumpMenuForm;
use Drupal\jump_menu\Form\JumpMenuOptions;

class FormController extends ControllerBase {

  public function form() {
    $form = new JumpMenuForm();
    $form_state = new FormState();
    $select = JumpMenuOptions::create();

    $select->addOption('Test', Url::fromRoute('test_page_test.test_page'));
    $select->addOption('Test2', Url::fromRoute('test_page_test.test_page'));

    $form_state->addBuildInfo('args', [$select]);

    return $this->formBuilder()->buildForm($form, $form_state);
  }

}
